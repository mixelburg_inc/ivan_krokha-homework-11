#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <mutex>
#include <vector>
#include <thread>
#include<sstream>

template <class T>
void printTimeDelta(const T timeDelta)
{
	// display time delta
	std::cout << "[+] Elapsed time in microseconds : "
		<< std::chrono::duration_cast<std::chrono::microseconds>(timeDelta).count()
		<< "qs" << std::endl;
	// display time delta
	std::cout << "[+] Elapsed time in milliseconds  : "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(timeDelta).count()
		<< " ms" << std::endl;
	// display time delta
	std::cout << "[+] Elapsed time in seconds : "
		<< std::chrono::duration_cast<std::chrono::seconds>(timeDelta).count()
		<< " sec" << std::endl << std::endl;
}

// Simple timer function
template <class T, class... Args>
void timeIt(T func, Args ... args)
{
	// get start time
	const auto start = std::chrono::steady_clock::now();

	func(args...);

	// get end time
	const auto end = std::chrono::steady_clock::now();
	// display time delta
	printTimeDelta(start - end);
}


void I_Love_Threads();
void call_I_Love_Threads();

void printVector(std::vector<int>& primes);

void getPrimes(int low, int high, std::vector<int>& primes);
std::vector<int> callGetPrimes(int low, int high);


class SynchronizedFile
{
public:
	SynchronizedFile(const std::string& path) : _path(path)
	{
		_file = new std::ofstream(path);
		if (!(_file->is_open())) throw std::exception("[!] Error opening the file");
	}

	~SynchronizedFile()
	{
		try
		{
			_file->close();
		}
		catch (std::exception& e)
		{
			std::cout << "[!] error closing the file" << std::endl;
		}

		delete _file;
	}

	void write(const std::string& dataToWrite)
	{
		std::lock_guard<std::mutex> lock(_writerMutex);
		*_file << dataToWrite;
	}

private:
	std::string _path;
	std::ofstream* _file;
	std::mutex _writerMutex;
};

class Writer
{
public:
	Writer(std::shared_ptr<SynchronizedFile> sf) : _sf(sf)
	{
	}

	void write(std::string data)
	{
		_sf->write(data);
	}

private:
	std::shared_ptr<SynchronizedFile> _sf;
};

void writePrimesToFile(int low, int high, Writer writer);
void callWritePrimesMultipleThreads(int begin, int end, std::string& filePath, int numThreads);
